EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J3
U 1 1 62009CD8
P 5450 5850
F 0 "J3" H 5500 6467 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 5500 6376 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Horizontal" H 5450 5850 50  0001 C CNN
F 3 "~" H 5450 5850 50  0001 C CNN
	1    5450 5850
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 62009CDE
P 5250 6250
F 0 "#PWR?" H 5250 6100 50  0001 C CNN
F 1 "+3V3" V 5250 6500 50  0000 C CNN
F 2 "" H 5250 6250 50  0001 C CNN
F 3 "" H 5250 6250 50  0001 C CNN
	1    5250 6250
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 62009CE4
P 5750 6150
F 0 "#PWR?" H 5750 6000 50  0001 C CNN
F 1 "+3V3" V 5750 6350 50  0000 C CNN
F 2 "" H 5750 6150 50  0001 C CNN
F 3 "" H 5750 6150 50  0001 C CNN
	1    5750 6150
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 62009CEA
P 5250 6050
F 0 "#PWR?" H 5250 5900 50  0001 C CNN
F 1 "+3V3" V 5250 6300 50  0000 C CNN
F 2 "" H 5250 6050 50  0001 C CNN
F 3 "" H 5250 6050 50  0001 C CNN
	1    5250 6050
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 62009CF0
P 5750 5950
F 0 "#PWR?" H 5750 5800 50  0001 C CNN
F 1 "+3V3" V 5750 6150 50  0000 C CNN
F 2 "" H 5750 5950 50  0001 C CNN
F 3 "" H 5750 5950 50  0001 C CNN
	1    5750 5950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62009CF6
P 5250 6350
F 0 "#PWR?" H 5250 6100 50  0001 C CNN
F 1 "GND" V 5250 6150 50  0000 C CNN
F 2 "" H 5250 6350 50  0001 C CNN
F 3 "" H 5250 6350 50  0001 C CNN
	1    5250 6350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62009CFC
P 5750 6350
F 0 "#PWR?" H 5750 6100 50  0001 C CNN
F 1 "GND" V 5750 6150 50  0000 C CNN
F 2 "" H 5750 6350 50  0001 C CNN
F 3 "" H 5750 6350 50  0001 C CNN
	1    5750 6350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62009D02
P 5750 6250
F 0 "#PWR?" H 5750 6000 50  0001 C CNN
F 1 "GND" V 5750 6050 50  0000 C CNN
F 2 "" H 5750 6250 50  0001 C CNN
F 3 "" H 5750 6250 50  0001 C CNN
	1    5750 6250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62009D08
P 5250 6150
F 0 "#PWR?" H 5250 5900 50  0001 C CNN
F 1 "GND" V 5250 5950 50  0000 C CNN
F 2 "" H 5250 6150 50  0001 C CNN
F 3 "" H 5250 6150 50  0001 C CNN
	1    5250 6150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62009D0E
P 5750 6050
F 0 "#PWR?" H 5750 5800 50  0001 C CNN
F 1 "GND" V 5750 5850 50  0000 C CNN
F 2 "" H 5750 6050 50  0001 C CNN
F 3 "" H 5750 6050 50  0001 C CNN
	1    5750 6050
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62009D14
P 5250 5950
F 0 "#PWR?" H 5250 5700 50  0001 C CNN
F 1 "GND" V 5250 5750 50  0000 C CNN
F 2 "" H 5250 5950 50  0001 C CNN
F 3 "" H 5250 5950 50  0001 C CNN
	1    5250 5950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62009D1A
P 5250 5850
F 0 "#PWR?" H 5250 5600 50  0001 C CNN
F 1 "GND" V 5250 5650 50  0000 C CNN
F 2 "" H 5250 5850 50  0001 C CNN
F 3 "" H 5250 5850 50  0001 C CNN
	1    5250 5850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62009D20
P 5750 5850
F 0 "#PWR?" H 5750 5600 50  0001 C CNN
F 1 "GND" V 5750 5650 50  0000 C CNN
F 2 "" H 5750 5850 50  0001 C CNN
F 3 "" H 5750 5850 50  0001 C CNN
	1    5750 5850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62009D26
P 5750 5450
F 0 "#PWR?" H 5750 5200 50  0001 C CNN
F 1 "GND" V 5750 5250 50  0000 C CNN
F 2 "" H 5750 5450 50  0001 C CNN
F 3 "" H 5750 5450 50  0001 C CNN
	1    5750 5450
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62009D2C
P 5250 5450
F 0 "#PWR?" H 5250 5200 50  0001 C CNN
F 1 "GND" V 5250 5250 50  0000 C CNN
F 2 "" H 5250 5450 50  0001 C CNN
F 3 "" H 5250 5450 50  0001 C CNN
	1    5250 5450
	0    1    1    0   
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 62009D32
P 5750 5550
F 0 "#PWR?" H 5750 5400 50  0001 C CNN
F 1 "+5VA" V 5750 5750 50  0000 C CNN
F 2 "" H 5750 5550 50  0001 C CNN
F 3 "" H 5750 5550 50  0001 C CNN
	1    5750 5550
	0    1    1    0   
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 62009D38
P 5250 5550
F 0 "#PWR?" H 5250 5400 50  0001 C CNN
F 1 "+5VA" V 5250 5800 50  0000 C CNN
F 2 "" H 5250 5550 50  0001 C CNN
F 3 "" H 5250 5550 50  0001 C CNN
	1    5250 5550
	0    -1   -1   0   
$EndComp
$Comp
L power:+9VA #PWR?
U 1 1 62009D3E
P 5250 5750
F 0 "#PWR?" H 5250 5625 50  0001 C CNN
F 1 "+9VA" V 5250 6000 50  0000 C CNN
F 2 "" H 5250 5750 50  0001 C CNN
F 3 "" H 5250 5750 50  0001 C CNN
	1    5250 5750
	0    -1   -1   0   
$EndComp
$Comp
L power:+9VA #PWR?
U 1 1 62009D44
P 5750 5750
F 0 "#PWR?" H 5750 5625 50  0001 C CNN
F 1 "+9VA" V 5750 5950 50  0000 C CNN
F 2 "" H 5750 5750 50  0001 C CNN
F 3 "" H 5750 5750 50  0001 C CNN
	1    5750 5750
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62009D4A
P 5750 5650
F 0 "#PWR?" H 5750 5400 50  0001 C CNN
F 1 "GND" V 5750 5450 50  0000 C CNN
F 2 "" H 5750 5650 50  0001 C CNN
F 3 "" H 5750 5650 50  0001 C CNN
	1    5750 5650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62009D50
P 5250 5650
F 0 "#PWR?" H 5250 5400 50  0001 C CNN
F 1 "GND" V 5250 5450 50  0000 C CNN
F 2 "" H 5250 5650 50  0001 C CNN
F 3 "" H 5250 5650 50  0001 C CNN
	1    5250 5650
	0    1    1    0   
$EndComp
Text Notes 5150 6500 0    50   ~ 0
Złączka do zasilania
Text Notes 5100 750  0    50   ~ 0
Złączka do płytki DAC
Text GLabel 3800 2500 2    50   Input ~ 0
SIG_2_4
Wire Wire Line
	3750 2800 3800 2800
Wire Wire Line
	3800 2700 3750 2700
Wire Wire Line
	3800 2600 3750 2600
Wire Wire Line
	3250 2600 3200 2600
Wire Wire Line
	3250 2700 3200 2700
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 62001C0D
P 3450 2700
F 0 "J1" H 3500 3117 50  0000 C CNN
F 1 "Conn_02x05_Horizontal" H 3500 3026 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Horizontal" H 3450 2700 50  0001 C CNN
F 3 "~" H 3450 2700 50  0001 C CNN
	1    3450 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2500 3750 2500
Text GLabel 3200 2500 0    50   Input ~ 0
SIG_1_4
Wire Wire Line
	3200 2500 3250 2500
Wire Wire Line
	3100 2800 3250 2800
Text GLabel 3200 2900 0    50   Input ~ 0
I2C1_SCL
Text GLabel 3800 2900 2    50   Input ~ 0
I2C1_SDA
$Comp
L power:GND #PWR?
U 1 1 62001C19
P 3800 2800
F 0 "#PWR?" H 3800 2550 50  0001 C CNN
F 1 "GND" V 3800 2600 50  0000 C CNN
F 2 "" H 3800 2800 50  0001 C CNN
F 3 "" H 3800 2800 50  0001 C CNN
	1    3800 2800
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62001C1F
P 3200 2700
F 0 "#PWR?" H 3200 2450 50  0001 C CNN
F 1 "GND" V 3200 2500 50  0000 C CNN
F 2 "" H 3200 2700 50  0001 C CNN
F 3 "" H 3200 2700 50  0001 C CNN
	1    3200 2700
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62001C25
P 3200 2600
F 0 "#PWR?" H 3200 2350 50  0001 C CNN
F 1 "GND" V 3200 2400 50  0000 C CNN
F 2 "" H 3200 2600 50  0001 C CNN
F 3 "" H 3200 2600 50  0001 C CNN
	1    3200 2600
	0    1    -1   0   
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 62001C2B
P 3800 2600
F 0 "#PWR?" H 3800 2450 50  0001 C CNN
F 1 "+5VA" V 3815 2728 50  0000 L CNN
F 2 "" H 3800 2600 50  0001 C CNN
F 3 "" H 3800 2600 50  0001 C CNN
	1    3800 2600
	0    1    1    0   
$EndComp
$Comp
L power:+9VA #PWR?
U 1 1 62001C31
P 3800 2700
F 0 "#PWR?" H 3800 2575 50  0001 C CNN
F 1 "+9VA" V 3815 2828 50  0000 L CNN
F 2 "" H 3800 2700 50  0001 C CNN
F 3 "" H 3800 2700 50  0001 C CNN
	1    3800 2700
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 62001C37
P 3100 2800
F 0 "#PWR?" H 3100 2650 50  0001 C CNN
F 1 "+3.3V" V 3115 2928 50  0000 L CNN
F 2 "" H 3100 2800 50  0001 C CNN
F 3 "" H 3100 2800 50  0001 C CNN
	1    3100 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3200 2900 3250 2900
Wire Wire Line
	3800 2900 3750 2900
Text Notes 3400 3100 0    50   ~ 0
VGA
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J4
U 1 1 6200B6FE
P 5500 1450
F 0 "J4" H 5550 2067 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 5550 1976 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical" H 5500 1450 50  0001 C CNN
F 3 "~" H 5500 1450 50  0001 C CNN
	1    5500 1450
	1    0    0    -1  
$EndComp
Text GLabel 5300 1150 0    50   Output ~ 0
SIG_1_1
Text GLabel 5800 1150 2    50   Output ~ 0
SIG_2_1
Text GLabel 5300 1250 0    50   Output ~ 0
SIG_1_2
Text GLabel 5800 1250 2    50   Output ~ 0
SIG_2_2
Text GLabel 5300 1350 0    50   Output ~ 0
SIG_1_3
Text GLabel 5800 1350 2    50   Output ~ 0
SIG_2_3
Text GLabel 5300 1450 0    50   Output ~ 0
SIG_1_4
Text GLabel 5800 1450 2    50   Output ~ 0
SIG_2_4
$Comp
L power:+5VA #PWR?
U 1 1 6200B70C
P 5800 1050
F 0 "#PWR?" H 5800 900 50  0001 C CNN
F 1 "+5VA" V 5800 1150 50  0000 L CNN
F 2 "" H 5800 1050 50  0001 C CNN
F 3 "" H 5800 1050 50  0001 C CNN
	1    5800 1050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6200B712
P 5300 1050
F 0 "#PWR?" H 5300 800 50  0001 C CNN
F 1 "GND" V 5305 922 50  0000 R CNN
F 2 "" H 5300 1050 50  0001 C CNN
F 3 "" H 5300 1050 50  0001 C CNN
	1    5300 1050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6200B718
P 5300 1550
F 0 "#PWR?" H 5300 1300 50  0001 C CNN
F 1 "GND" V 5305 1422 50  0000 R CNN
F 2 "" H 5300 1550 50  0001 C CNN
F 3 "" H 5300 1550 50  0001 C CNN
	1    5300 1550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6200B71E
P 5300 1950
F 0 "#PWR?" H 5300 1700 50  0001 C CNN
F 1 "GND" V 5305 1822 50  0000 R CNN
F 2 "" H 5300 1950 50  0001 C CNN
F 3 "" H 5300 1950 50  0001 C CNN
	1    5300 1950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6200B724
P 5300 1650
F 0 "#PWR?" H 5300 1400 50  0001 C CNN
F 1 "GND" V 5305 1522 50  0000 R CNN
F 2 "" H 5300 1650 50  0001 C CNN
F 3 "" H 5300 1650 50  0001 C CNN
	1    5300 1650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6200B72A
P 5800 1550
F 0 "#PWR?" H 5800 1300 50  0001 C CNN
F 1 "GND" V 5805 1422 50  0000 R CNN
F 2 "" H 5800 1550 50  0001 C CNN
F 3 "" H 5800 1550 50  0001 C CNN
	1    5800 1550
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6200B730
P 5800 1650
F 0 "#PWR?" H 5800 1400 50  0001 C CNN
F 1 "GND" V 5805 1522 50  0000 R CNN
F 2 "" H 5800 1650 50  0001 C CNN
F 3 "" H 5800 1650 50  0001 C CNN
	1    5800 1650
	0    -1   -1   0   
$EndComp
Text GLabel 5800 1750 2    50   BiDi ~ 0
I2C1_SDA
Text GLabel 5300 1750 0    50   Input ~ 0
I2C1_SCL
$Comp
L power:+3.3V #PWR?
U 1 1 6200B738
P 5300 1850
F 0 "#PWR?" H 5300 1700 50  0001 C CNN
F 1 "+3.3V" V 5300 1950 50  0000 L CNN
F 2 "" H 5300 1850 50  0001 C CNN
F 3 "" H 5300 1850 50  0001 C CNN
	1    5300 1850
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 6200B73E
P 5800 1850
F 0 "#PWR?" H 5800 1700 50  0001 C CNN
F 1 "+3.3V" V 5800 1950 50  0000 L CNN
F 2 "" H 5800 1850 50  0001 C CNN
F 3 "" H 5800 1850 50  0001 C CNN
	1    5800 1850
	0    1    -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 6200B744
P 5800 1950
F 0 "#PWR?" H 5800 1800 50  0001 C CNN
F 1 "+3.3V" V 5800 2050 50  0000 L CNN
F 2 "" H 5800 1950 50  0001 C CNN
F 3 "" H 5800 1950 50  0001 C CNN
	1    5800 1950
	0    1    -1   0   
$EndComp
Text Notes 5100 2100 0    50   ~ 0
i2c1 pullupy na łączniku
Wire Wire Line
	7950 5750 8000 5750
Wire Wire Line
	8000 5650 7950 5650
Wire Wire Line
	7450 5550 7400 5550
Wire Wire Line
	7450 5650 7400 5650
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J9
U 1 1 6201B373
P 7650 5650
F 0 "J9" H 7700 6067 50  0000 C CNN
F 1 "Conn_02x05_Horizontal" H 7700 5976 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Horizontal" H 7650 5650 50  0001 C CNN
F 3 "~" H 7650 5650 50  0001 C CNN
	1    7650 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 5750 7450 5750
Text GLabel 7400 5850 0    50   Input ~ 0
I2C0_SCL
Text GLabel 8000 5850 2    50   Input ~ 0
I2C0_SDA
$Comp
L power:GND #PWR?
U 1 1 6201B385
P 7400 5650
F 0 "#PWR?" H 7400 5400 50  0001 C CNN
F 1 "GND" V 7400 5450 50  0000 C CNN
F 2 "" H 7400 5650 50  0001 C CNN
F 3 "" H 7400 5650 50  0001 C CNN
	1    7400 5650
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6201B38B
P 7400 5550
F 0 "#PWR?" H 7400 5300 50  0001 C CNN
F 1 "GND" V 7400 5350 50  0000 C CNN
F 2 "" H 7400 5550 50  0001 C CNN
F 3 "" H 7400 5550 50  0001 C CNN
	1    7400 5550
	0    1    -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 6201B39D
P 7300 5750
F 0 "#PWR?" H 7300 5600 50  0001 C CNN
F 1 "+3.3V" V 7300 5850 50  0000 L CNN
F 2 "" H 7300 5750 50  0001 C CNN
F 3 "" H 7300 5750 50  0001 C CNN
	1    7300 5750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7400 5850 7450 5850
Wire Wire Line
	8000 5850 7950 5850
Text Notes 6900 6000 0    50   ~ 0
Złączka do opcjonalnej płytki z kontrolkami
$Comp
L Jumper:SolderJumper_2_Bridged JP1
U 1 1 6201BE89
P 6750 5450
F 0 "JP1" H 6750 5655 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 6750 5564 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm" H 6750 5450 50  0001 C CNN
F 3 "~" H 6750 5450 50  0001 C CNN
	1    6750 5450
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP2
U 1 1 6201D64E
P 8750 5450
F 0 "JP2" H 8750 5655 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 8750 5564 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm" H 8750 5450 50  0001 C CNN
F 3 "~" H 8750 5450 50  0001 C CNN
	1    8750 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 5450 7450 5450
$Comp
L power:GND #PWR?
U 1 1 620201D6
P 8900 5550
F 0 "#PWR?" H 8900 5300 50  0001 C CNN
F 1 "GND" V 8900 5350 50  0000 C CNN
F 2 "" H 8900 5550 50  0001 C CNN
F 3 "" H 8900 5550 50  0001 C CNN
	1    8900 5550
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62020599
P 8900 5450
F 0 "#PWR?" H 8900 5200 50  0001 C CNN
F 1 "GND" V 8900 5250 50  0000 C CNN
F 2 "" H 8900 5450 50  0001 C CNN
F 3 "" H 8900 5450 50  0001 C CNN
	1    8900 5450
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62020852
P 6600 5450
F 0 "#PWR?" H 6600 5200 50  0001 C CNN
F 1 "GND" V 6600 5250 50  0000 C CNN
F 2 "" H 6600 5450 50  0001 C CNN
F 3 "" H 6600 5450 50  0001 C CNN
	1    6600 5450
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62020F60
P 8000 5650
F 0 "#PWR?" H 8000 5400 50  0001 C CNN
F 1 "GND" V 8000 5450 50  0000 C CNN
F 2 "" H 8000 5650 50  0001 C CNN
F 3 "" H 8000 5650 50  0001 C CNN
	1    8000 5650
	0    -1   1    0   
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP3
U 1 1 6201E29A
P 8750 5550
F 0 "JP3" H 8750 5750 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 8750 5650 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm" H 8750 5550 50  0001 C CNN
F 3 "~" H 8750 5550 50  0001 C CNN
	1    8750 5550
	1    0    0    1   
$EndComp
Wire Wire Line
	7950 5450 8600 5450
Wire Wire Line
	7950 5550 8600 5550
$Comp
L power:+3.3V #PWR?
U 1 1 62024134
P 8000 5750
F 0 "#PWR?" H 8000 5600 50  0001 C CNN
F 1 "+3.3V" V 8000 5850 50  0000 L CNN
F 2 "" H 8000 5750 50  0001 C CNN
F 3 "" H 8000 5750 50  0001 C CNN
	1    8000 5750
	0    1    1    0   
$EndComp
Text Notes 7050 5150 0    50   ~ 0
Bridge gdyby np SPI trzeba było dołożyć
$Comp
L Device:R R1
U 1 1 62025E06
P 5300 3800
F 0 "R1" H 5370 3846 50  0000 L CNN
F 1 "4.7k" H 5370 3755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5230 3800 50  0001 C CNN
F 3 "~" H 5300 3800 50  0001 C CNN
	1    5300 3800
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 6202646C
P 5600 3800
F 0 "R3" H 5670 3846 50  0000 L CNN
F 1 "4.7k" H 5670 3755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5530 3800 50  0001 C CNN
F 3 "~" H 5600 3800 50  0001 C CNN
	1    5600 3800
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 62027533
P 5300 3650
F 0 "#PWR?" H 5300 3500 50  0001 C CNN
F 1 "+3.3V" H 5315 3823 50  0000 C CNN
F 2 "" H 5300 3650 50  0001 C CNN
F 3 "" H 5300 3650 50  0001 C CNN
	1    5300 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 62027886
P 5600 3650
F 0 "#PWR?" H 5600 3500 50  0001 C CNN
F 1 "+3.3V" H 5615 3823 50  0000 C CNN
F 2 "" H 5600 3650 50  0001 C CNN
F 3 "" H 5600 3650 50  0001 C CNN
	1    5600 3650
	1    0    0    -1  
$EndComp
Text GLabel 5300 3000 0    50   Input ~ 0
I2C0_SDA
Text GLabel 5300 2900 0    50   Input ~ 0
I2C0_SCL
Text GLabel 5250 4000 0    50   Input ~ 0
I2C1_SCL
Text GLabel 5250 4100 0    50   Input ~ 0
I2C1_SDA
Text GLabel 3800 4050 2    50   Input ~ 0
SIG_2_3
Wire Wire Line
	3750 4350 3800 4350
Wire Wire Line
	3800 4250 3750 4250
Wire Wire Line
	3800 4150 3750 4150
Wire Wire Line
	3250 4150 3200 4150
Wire Wire Line
	3250 4250 3200 4250
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 6205B926
P 3450 4250
F 0 "J2" H 3500 4667 50  0000 C CNN
F 1 "Conn_02x05_Horizontal" H 3500 4576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Horizontal" H 3450 4250 50  0001 C CNN
F 3 "~" H 3450 4250 50  0001 C CNN
	1    3450 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 4050 3750 4050
Text GLabel 3200 4050 0    50   Input ~ 0
SIG_1_3
Wire Wire Line
	3200 4050 3250 4050
Wire Wire Line
	3100 4350 3250 4350
Text GLabel 3200 4450 0    50   Input ~ 0
I2C1_SCL
Text GLabel 3800 4450 2    50   Input ~ 0
I2C1_SDA
$Comp
L power:GND #PWR?
U 1 1 6205B932
P 3800 4350
F 0 "#PWR?" H 3800 4100 50  0001 C CNN
F 1 "GND" V 3800 4150 50  0000 C CNN
F 2 "" H 3800 4350 50  0001 C CNN
F 3 "" H 3800 4350 50  0001 C CNN
	1    3800 4350
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6205B938
P 3200 4250
F 0 "#PWR?" H 3200 4000 50  0001 C CNN
F 1 "GND" V 3200 4050 50  0000 C CNN
F 2 "" H 3200 4250 50  0001 C CNN
F 3 "" H 3200 4250 50  0001 C CNN
	1    3200 4250
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6205B93E
P 3200 4150
F 0 "#PWR?" H 3200 3900 50  0001 C CNN
F 1 "GND" V 3200 3950 50  0000 C CNN
F 2 "" H 3200 4150 50  0001 C CNN
F 3 "" H 3200 4150 50  0001 C CNN
	1    3200 4150
	0    1    -1   0   
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 6205B944
P 3800 4150
F 0 "#PWR?" H 3800 4000 50  0001 C CNN
F 1 "+5VA" V 3815 4278 50  0000 L CNN
F 2 "" H 3800 4150 50  0001 C CNN
F 3 "" H 3800 4150 50  0001 C CNN
	1    3800 4150
	0    1    1    0   
$EndComp
$Comp
L power:+9VA #PWR?
U 1 1 6205B94A
P 3800 4250
F 0 "#PWR?" H 3800 4125 50  0001 C CNN
F 1 "+9VA" V 3815 4378 50  0000 L CNN
F 2 "" H 3800 4250 50  0001 C CNN
F 3 "" H 3800 4250 50  0001 C CNN
	1    3800 4250
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 6205B950
P 3100 4350
F 0 "#PWR?" H 3100 4200 50  0001 C CNN
F 1 "+3.3V" V 3115 4478 50  0000 L CNN
F 2 "" H 3100 4350 50  0001 C CNN
F 3 "" H 3100 4350 50  0001 C CNN
	1    3100 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3200 4450 3250 4450
Wire Wire Line
	3800 4450 3750 4450
Text Notes 3400 4650 0    50   ~ 0
VGA
Text GLabel 7600 2500 2    50   Input ~ 0
SIG_2_2
Wire Wire Line
	7550 2800 7600 2800
Wire Wire Line
	7600 2700 7550 2700
Wire Wire Line
	7600 2600 7550 2600
Wire Wire Line
	7050 2600 7000 2600
Wire Wire Line
	7050 2700 7000 2700
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J8
U 1 1 62063B24
P 7250 2700
F 0 "J8" H 7300 3117 50  0000 C CNN
F 1 "Conn_02x05_Horizontal" H 7300 3026 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Horizontal" H 7250 2700 50  0001 C CNN
F 3 "~" H 7250 2700 50  0001 C CNN
	1    7250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 2500 7550 2500
Text GLabel 7000 2500 0    50   Input ~ 0
SIG_1_2
Wire Wire Line
	7000 2500 7050 2500
Wire Wire Line
	6900 2800 7050 2800
Text GLabel 7000 2900 0    50   Input ~ 0
I2C1_SCL
Text GLabel 7600 2900 2    50   Input ~ 0
I2C1_SDA
$Comp
L power:GND #PWR?
U 1 1 62063B30
P 7600 2800
F 0 "#PWR?" H 7600 2550 50  0001 C CNN
F 1 "GND" V 7600 2600 50  0000 C CNN
F 2 "" H 7600 2800 50  0001 C CNN
F 3 "" H 7600 2800 50  0001 C CNN
	1    7600 2800
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62063B36
P 7000 2700
F 0 "#PWR?" H 7000 2450 50  0001 C CNN
F 1 "GND" V 7000 2500 50  0000 C CNN
F 2 "" H 7000 2700 50  0001 C CNN
F 3 "" H 7000 2700 50  0001 C CNN
	1    7000 2700
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62063B3C
P 7000 2600
F 0 "#PWR?" H 7000 2350 50  0001 C CNN
F 1 "GND" V 7000 2400 50  0000 C CNN
F 2 "" H 7000 2600 50  0001 C CNN
F 3 "" H 7000 2600 50  0001 C CNN
	1    7000 2600
	0    1    -1   0   
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 62063B42
P 7600 2600
F 0 "#PWR?" H 7600 2450 50  0001 C CNN
F 1 "+5VA" V 7615 2728 50  0000 L CNN
F 2 "" H 7600 2600 50  0001 C CNN
F 3 "" H 7600 2600 50  0001 C CNN
	1    7600 2600
	0    1    1    0   
$EndComp
$Comp
L power:+9VA #PWR?
U 1 1 62063B48
P 7600 2700
F 0 "#PWR?" H 7600 2575 50  0001 C CNN
F 1 "+9VA" V 7615 2828 50  0000 L CNN
F 2 "" H 7600 2700 50  0001 C CNN
F 3 "" H 7600 2700 50  0001 C CNN
	1    7600 2700
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 62063B4E
P 6900 2800
F 0 "#PWR?" H 6900 2650 50  0001 C CNN
F 1 "+3.3V" V 6915 2928 50  0000 L CNN
F 2 "" H 6900 2800 50  0001 C CNN
F 3 "" H 6900 2800 50  0001 C CNN
	1    6900 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7000 2900 7050 2900
Wire Wire Line
	7600 2900 7550 2900
Text Notes 7200 3100 0    50   ~ 0
VGA
Text GLabel 7550 4050 2    50   Input ~ 0
SIG_2_1
Wire Wire Line
	7500 4350 7550 4350
Wire Wire Line
	7550 4250 7500 4250
Wire Wire Line
	7550 4150 7500 4150
Wire Wire Line
	7000 4150 6950 4150
Wire Wire Line
	7000 4250 6950 4250
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J7
U 1 1 6206890F
P 7200 4250
F 0 "J7" H 7250 4667 50  0000 C CNN
F 1 "Conn_02x05_Horizontal" H 7250 4576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Horizontal" H 7200 4250 50  0001 C CNN
F 3 "~" H 7200 4250 50  0001 C CNN
	1    7200 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 4050 7500 4050
Text GLabel 6950 4050 0    50   Input ~ 0
SIG_1_1
Wire Wire Line
	6950 4050 7000 4050
Wire Wire Line
	6850 4350 7000 4350
Text GLabel 6950 4450 0    50   Input ~ 0
I2C1_SCL
Text GLabel 7550 4450 2    50   Input ~ 0
I2C1_SDA
$Comp
L power:GND #PWR?
U 1 1 6206891B
P 7550 4350
F 0 "#PWR?" H 7550 4100 50  0001 C CNN
F 1 "GND" V 7550 4150 50  0000 C CNN
F 2 "" H 7550 4350 50  0001 C CNN
F 3 "" H 7550 4350 50  0001 C CNN
	1    7550 4350
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62068921
P 6950 4250
F 0 "#PWR?" H 6950 4000 50  0001 C CNN
F 1 "GND" V 6950 4050 50  0000 C CNN
F 2 "" H 6950 4250 50  0001 C CNN
F 3 "" H 6950 4250 50  0001 C CNN
	1    6950 4250
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62068927
P 6950 4150
F 0 "#PWR?" H 6950 3900 50  0001 C CNN
F 1 "GND" V 6950 3950 50  0000 C CNN
F 2 "" H 6950 4150 50  0001 C CNN
F 3 "" H 6950 4150 50  0001 C CNN
	1    6950 4150
	0    1    -1   0   
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 6206892D
P 7550 4150
F 0 "#PWR?" H 7550 4000 50  0001 C CNN
F 1 "+5VA" V 7565 4278 50  0000 L CNN
F 2 "" H 7550 4150 50  0001 C CNN
F 3 "" H 7550 4150 50  0001 C CNN
	1    7550 4150
	0    1    1    0   
$EndComp
$Comp
L power:+9VA #PWR?
U 1 1 62068933
P 7550 4250
F 0 "#PWR?" H 7550 4125 50  0001 C CNN
F 1 "+9VA" V 7565 4378 50  0000 L CNN
F 2 "" H 7550 4250 50  0001 C CNN
F 3 "" H 7550 4250 50  0001 C CNN
	1    7550 4250
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 62068939
P 6850 4350
F 0 "#PWR?" H 6850 4200 50  0001 C CNN
F 1 "+3.3V" V 6865 4478 50  0000 L CNN
F 2 "" H 6850 4350 50  0001 C CNN
F 3 "" H 6850 4350 50  0001 C CNN
	1    6850 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6950 4450 7000 4450
Wire Wire Line
	7550 4450 7500 4450
Text Notes 7150 4650 0    50   ~ 0
VGA
$Comp
L Connector:Conn_01x03_Male J5
U 1 1 620741E3
P 5850 4100
F 0 "J5" H 5822 4032 50  0000 R CNN
F 1 "Conn_01x03_Male" H 5822 4123 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5850 4100 50  0001 C CNN
F 3 "~" H 5850 4100 50  0001 C CNN
	1    5850 4100
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62076464
P 5600 4250
F 0 "#PWR?" H 5600 4000 50  0001 C CNN
F 1 "GND" H 5605 4077 50  0000 C CNN
F 2 "" H 5600 4250 50  0001 C CNN
F 3 "" H 5600 4250 50  0001 C CNN
	1    5600 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4250 5600 4200
Wire Wire Line
	5600 4200 5650 4200
Wire Wire Line
	5600 3950 5600 4000
Wire Wire Line
	5600 4000 5650 4000
Wire Wire Line
	5650 4100 5300 4100
Wire Wire Line
	5300 4100 5300 3950
Wire Wire Line
	5600 4000 5250 4000
Connection ~ 5600 4000
Wire Wire Line
	5300 4100 5250 4100
Connection ~ 5300 4100
$Comp
L Device:R R2
U 1 1 6208480B
P 5350 2700
F 0 "R2" H 5420 2746 50  0000 L CNN
F 1 "4.7k" H 5420 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5280 2700 50  0001 C CNN
F 3 "~" H 5350 2700 50  0001 C CNN
	1    5350 2700
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4
U 1 1 62084811
P 5650 2700
F 0 "R4" H 5720 2746 50  0000 L CNN
F 1 "4.7k" H 5720 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5580 2700 50  0001 C CNN
F 3 "~" H 5650 2700 50  0001 C CNN
	1    5650 2700
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 62084817
P 5350 2550
F 0 "#PWR?" H 5350 2400 50  0001 C CNN
F 1 "+3.3V" H 5365 2723 50  0000 C CNN
F 2 "" H 5350 2550 50  0001 C CNN
F 3 "" H 5350 2550 50  0001 C CNN
	1    5350 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 6208481D
P 5650 2550
F 0 "#PWR?" H 5650 2400 50  0001 C CNN
F 1 "+3.3V" H 5665 2723 50  0000 C CNN
F 2 "" H 5650 2550 50  0001 C CNN
F 3 "" H 5650 2550 50  0001 C CNN
	1    5650 2550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J6
U 1 1 62084823
P 5900 3000
F 0 "J6" H 5872 2932 50  0000 R CNN
F 1 "Conn_01x03_Male" H 5872 3023 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5900 3000 50  0001 C CNN
F 3 "~" H 5900 3000 50  0001 C CNN
	1    5900 3000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62084829
P 5650 3150
F 0 "#PWR?" H 5650 2900 50  0001 C CNN
F 1 "GND" H 5655 2977 50  0000 C CNN
F 2 "" H 5650 3150 50  0001 C CNN
F 3 "" H 5650 3150 50  0001 C CNN
	1    5650 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3150 5650 3100
Wire Wire Line
	5650 3100 5700 3100
Wire Wire Line
	5650 2850 5650 2900
Wire Wire Line
	5650 2900 5700 2900
Wire Wire Line
	5700 3000 5350 3000
Wire Wire Line
	5350 3000 5350 2850
Wire Wire Line
	5650 2900 5300 2900
Connection ~ 5650 2900
Wire Wire Line
	5350 3000 5300 3000
Connection ~ 5350 3000
Text Notes 5150 3350 0    50   ~ 0
I2C do\nRPI x2
$EndSCHEMATC
